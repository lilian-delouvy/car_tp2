package fr.carmiage.tp2_car.models;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;
@Entity
public class Commande {
    public Commande() {
    }

    public Commande(List<CommandeLigne> commandeLignes){
        this.commandeLignes = new ArrayList<>();
    }
    public Date date = new Date();

    @Id @GeneratedValue long id;
    @OneToMany(cascade= CascadeType.ALL)
    private List<CommandeLigne> commandeLignes;

    @Override
    public String toString() {
        SimpleDateFormat formater = new SimpleDateFormat("'le' dd-MM-yyyy 'à' hh:mm:ss");
        return "Commande{" +
                "id=" + id +
                ", commandeLignes=" + commandeLignes +
                ", date=" + formater.format(date) +
                '}';
    }

    public long getId() {
        return id;
    }

    public List<CommandeLigne> getCommandeLignes() {
        return commandeLignes;
    }

    public void setCommandeLignes(List<CommandeLigne> commandeLignes) {
        this.commandeLignes = commandeLignes;
    }

    public float getTotal(){
        float result = 0;
        for(CommandeLigne ligne: commandeLignes){
            result += ligne.getPrix();
        }
        return result;
    }
}
