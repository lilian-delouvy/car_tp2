package fr.carmiage.tp2_car.models;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Panier {

    public Panier(){
        commandeLignes = new ArrayList<>();
    }

    @Id @GeneratedValue(strategy=GenerationType.IDENTITY) long id;
    @OneToMany(cascade= CascadeType.ALL)
    private List<CommandeLigne> commandeLignes;

    private void addLigne(CommandeLigne ligne){
        commandeLignes.add(ligne);
    }

    public String addLigne(Produit p,int qt, int spQuantite){
        for (CommandeLigne cl:commandeLignes
             ) {
                if (cl.getProduit()==p){
                    if(cl.getQuantite() + qt > spQuantite)
                        return "La quantité totale est supérieure au stock !";
                    cl.setQuantite(cl.getQuantite()+qt);
                    return null;
                }

            }

        commandeLignes.add(new CommandeLigne(p,qt));
        return null;
    }
    public void removePanier(Produit p){
        CommandeLigne ligneAModifier=null;
        for (CommandeLigne cl: commandeLignes
        ) {
            if(cl.getProduit()==p){
                 ligneAModifier=cl;

            }
        }
        if(ligneAModifier!=null){
            commandeLignes.remove(ligneAModifier);

        }

    }
    public float getTotal(){
        float total = 0;
        for(CommandeLigne ligne: commandeLignes){
            total += ligne.getPrix();
        }
        return total;
    }

    public List<CommandeLigne> getCommandeLignes(){
        return commandeLignes;
    }
}
