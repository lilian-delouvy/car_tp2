package fr.carmiage.tp2_car.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class Produit implements Serializable {

    @Id @GeneratedValue long id;
    private String nom;
    private float prix;
    private boolean disponible;

    public String toString(){
        return "id: "+id+" nom "+nom+ " prix "+prix;
    }

    public Produit(){

    }
    public Produit(int id,String nom){
        this.id=id;
        this.nom=nom;
    }
    public Produit(int id,String nom,boolean disponible,float prix){
        this.id=id;
        this.nom=nom;
        this.prix=prix;
        this.disponible=disponible;
    }



    public long getId(){
        return this.id;
    }

    public String getNom(){
        return this.nom;
    }
    public float getPrix() {
        return prix;
    }
}
