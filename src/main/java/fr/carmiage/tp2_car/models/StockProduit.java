package fr.carmiage.tp2_car.models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class StockProduit implements Serializable  {

    @Id @GeneratedValue long id;
    @OneToOne(cascade= CascadeType.ALL)
    private Produit produit;
    private int quantite;

    public Produit getProduit() {
        return produit;
    }

    public void setProduit(Produit produit) {
        this.produit = produit;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }
}
