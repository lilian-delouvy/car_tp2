package fr.carmiage.tp2_car.models;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.annotation.security.DeclareRoles;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
@DeclareRoles("USER")
public class Client implements UserDetails{
    @Id @GeneratedValue(strategy=GenerationType.IDENTITY) long id;
    private String nom;

    @OneToOne(cascade=CascadeType.ALL)
    private Panier panier;
    @OneToMany(cascade=CascadeType.ALL)
    private List<Commande> commandesList;
    @Column(unique = true)
    private String email;
    private String password;

    public Client(Client user) {
        this.email=user.email;
        this.password=user.password;
        this.nom=user.nom;
        this.panier=new Panier();
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", panier=" + panier +
                ", commandesList=" + commandesList +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Panier getPanier() {
        return panier;
    }

    public void setPanier(Panier panier) {
        this.panier = panier;
    }

    public List<Commande> getCommandesList() {
        return commandesList;
    }

    public void setCommandesList(List<Commande> commandesList) {
        this.commandesList = commandesList;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Client(String nom){
        this.nom = nom;
        this.panier = new Panier();
        this.commandesList = new ArrayList<>();
    }

    public Client() {

    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<SimpleGrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("Client"));
        return authorities;    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

}
