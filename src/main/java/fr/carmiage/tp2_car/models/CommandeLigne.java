package fr.carmiage.tp2_car.models;

import javax.persistence.*;

@Entity
public class CommandeLigne {

    public CommandeLigne(Produit produit, int quantite) {
        this.produit = produit;
        this.quantite = quantite;
    }
    @Id @GeneratedValue long id;
    @OneToOne(cascade= CascadeType.ALL) private  Produit produit;
    private int quantite;

    public CommandeLigne() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setProduit(Produit produit) {
        this.produit = produit;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public Produit getProduit(){
        return produit;
    }
    public int getQuantite(){
        return quantite;
    }
    public float getPrix() {
        return produit.getPrix() * quantite;
    }
}
