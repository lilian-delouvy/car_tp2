package fr.carmiage.tp2_car.service;

import fr.carmiage.tp2_car.models.Client;
import fr.carmiage.tp2_car.repositories.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClientService implements UserDetailsService {

   @Autowired
    private ClientRepository clientRepo;


    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Client user = clientRepo.findByEmail(s);
        if (user == null) {
            throw new UsernameNotFoundException(s);
        }
        return new Client(user);
    }

}
