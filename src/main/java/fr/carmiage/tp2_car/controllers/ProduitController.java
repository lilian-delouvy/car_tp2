package fr.carmiage.tp2_car.controllers;

import fr.carmiage.tp2_car.models.Produit;
import fr.carmiage.tp2_car.repositories.ProduitRepository;
import fr.carmiage.tp2_car.repositories.StockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Optional;

@Controller
public class ProduitController {

    @Autowired
    private StockRepository stockRepository;

    @Autowired
    private ProduitRepository produitRepository;

    @GetMapping("/listeProduit")
    public String listeProduit(Model model){
        model.addAttribute("listeStock",stockRepository.findAllWhereStockPos().get());
        return "listeProduit";
    }

    @GetMapping("/detailProduit/{id}")
    public String detailProduit(Model model,@PathVariable String id){
        Optional element = produitRepository.findById(Long.valueOf(id));
        Produit product = (Produit)element.get();
        model.addAttribute("produit", product);
        model.addAttribute("stock", stockRepository.findByProduct(product).get());
        return "detailProduit";
    }

    @GetMapping("/ordersList")
    public String ordersList(){
        return "ordersList";
    }

    @PostMapping("/detailProduit/{id}/ajouter")
    public String ajoutProduit(Model model,@PathVariable String id){
        model.addAttribute("produit", produitRepository.findById(Integer.valueOf(id)));
        return "listeProduit";
    }

}
