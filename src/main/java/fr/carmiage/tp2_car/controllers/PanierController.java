package fr.carmiage.tp2_car.controllers;
import fr.carmiage.tp2_car.models.*;
import fr.carmiage.tp2_car.repositories.ClientRepository;
import fr.carmiage.tp2_car.repositories.ProduitRepository;
import fr.carmiage.tp2_car.repositories.StockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Optional;

@Controller
public class PanierController {

    @Autowired
    private StockRepository stockRepository;
    @Autowired
    private ProduitRepository produitRepository;
    @Autowired
    private ClientRepository clientRepository;
    @GetMapping("/panier")
    public ModelAndView panierVue(Principal principal,ModelAndView mv, @ModelAttribute("error") final String error) {
        if(error != null){
            System.out.println(error);
            mv.addObject("error", error);
        }
        Client user = (Client) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Client client=clientRepository.findByEmail(user.getEmail());
        mv.addObject("liste_ligne",client.getPanier().getCommandeLignes());
        mv.addObject("total", client.getPanier().getTotal());
        mv.setViewName("cart");
        return mv;
    }

    @PostMapping("/ajoutPanier")
    public String ajoutPanier(Principal principal, @RequestParam("id_produit") String id_produit, @RequestParam("quantite") String quantite, ModelAndView mv, RedirectAttributes redirectAttributes) {
        Produit p =produitRepository.findById(Long.valueOf(id_produit)).get();
        StockProduit sp = stockRepository.findByProduct(p).get();
        if((sp.getQuantite()-Integer.parseInt(quantite)) < 0){
            redirectAttributes.addFlashAttribute("error", "Erreur: la quantité en stock de ce produit est de " + sp.getQuantite());
            return "redirect:/panier";
        }
        Client c = (Client) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Client client2=clientRepository.findByEmail(c.getEmail());
        if(Integer.parseInt(quantite) > 0){
            String message = client2.getPanier().addLigne(p,Integer.parseInt(quantite), sp.getQuantite());
            if(message != null){
                redirectAttributes.addFlashAttribute("error", message);
            }
            clientRepository.save(client2);
        }
        return "redirect:/panier";
    }

    @PostMapping("/supprimerLignePanier")
    public String supprimerLignePanier( @RequestParam("id_produit") String id_produit,RedirectAttributes redirectAttributes){
        Client connecte = (Client) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Client client2=clientRepository.findByEmail(connecte.getEmail());

        Panier pa=client2.getPanier();

        Produit p=produitRepository.findById(Long.valueOf(id_produit)).get();

        pa.removePanier(p);
        clientRepository.save(client2);
        return "redirect:/panier";

    }

    @PostMapping("/confirmationPanier")
    public String confirmationPanier( RedirectAttributes redirectAttributes){
        Client connecte = (Client) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Client client2=clientRepository.findByEmail(connecte.getEmail());

        Panier pa=client2.getPanier();
        Commande commande= new Commande();

        if(pa.getCommandeLignes().size()==0){
            redirectAttributes.addFlashAttribute("error", "Panier vide");
            return "redirect:/panier";
        }

        for (CommandeLigne commandeLigne: pa.getCommandeLignes()
             ) {
            Produit p=commandeLigne.getProduit();
            StockProduit sp=stockRepository.findByProduct(p).get();
                if(commandeLigne.getQuantite()>sp.getQuantite()){
                    redirectAttributes.addFlashAttribute("error", "La quantite en stock n'est pas suffisante");
                    return "redirect:/panier";
                }
                else {
                    sp.setQuantite(sp.getQuantite()-commandeLigne.getQuantite());
                    stockRepository.save(sp);
                }
        }
        commande.setCommandeLignes(pa.getCommandeLignes());
        client2.getCommandesList().add(commande);
        client2.setPanier(new Panier());

        clientRepository.save(client2);
        redirectAttributes.addFlashAttribute("command_confirmation", "Votre commande a été passée.");
        return "redirect:/";
    }
}
