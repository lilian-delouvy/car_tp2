package fr.carmiage.tp2_car.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class RootController {

    @GetMapping("/")
    public ModelAndView serverRoot(ModelAndView mv, @ModelAttribute("command_confirmation") final String confirmation){
        if(confirmation != null){
            mv.addObject("confirmation", confirmation);
        }
        mv.setViewName("index");
        return mv;
    }


}
