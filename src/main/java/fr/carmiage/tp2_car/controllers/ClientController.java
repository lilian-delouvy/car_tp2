package fr.carmiage.tp2_car.controllers;

import fr.carmiage.tp2_car.models.Client;
import fr.carmiage.tp2_car.models.Panier;
import fr.carmiage.tp2_car.repositories.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class ClientController {

    @Autowired
    ClientRepository clientRepository;

    @GetMapping("/inscription")
    public ModelAndView inscriptionView(ModelAndView model, @ModelAttribute("error") final String error) {
        if(error != null){
            System.out.println(error);
            model.addObject("error", error);
        }
        model.setViewName("inscription");
        return model;
    }

    @PostMapping("/inscription")
    public ModelAndView inscription(@ModelAttribute("client") Client c, BindingResult res, ModelAndView model, RedirectAttributes redirectAttributes) {

        BCryptPasswordEncoder encoder =new BCryptPasswordEncoder();

        if(clientRepository.findByEmail(c.getEmail()) == null){
            c.setPassword(encoder.encode(c.getPassword()));
            c.setPanier(new Panier());
            clientRepository.save(c);
            model.setViewName("redirect:/login");
        }
        else{
            redirectAttributes.addFlashAttribute("error", "L'adresse email est déjà utilisée !");
            model.setViewName("redirect:/inscription");
        }
        return model;

    }

    @GetMapping("/login")
    public ModelAndView loginView(ModelAndView model){
        model.setViewName("login");
        return model;
    }

}
