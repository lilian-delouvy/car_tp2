package fr.carmiage.tp2_car.controllers;

import fr.carmiage.tp2_car.models.Client;
import fr.carmiage.tp2_car.models.Commande;
import fr.carmiage.tp2_car.models.Produit;
import fr.carmiage.tp2_car.repositories.ClientRepository;
import fr.carmiage.tp2_car.repositories.CommandeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;

@Controller
public class CommandeController {

    @Autowired
     ClientRepository clientRepository;
    @Autowired
    CommandeRepository commandeRepository;


    @GetMapping("/detailCommande")
    public String detailProduit(Model model,@RequestParam("id_commande") String id_commande){
        Optional element = commandeRepository.findById(Long.valueOf(id_commande));
        Commande commande= (Commande) element.get();
        model.addAttribute("commande", commande);
        return "detailCommande";
    }

    @GetMapping("/listeCommande")
    public String listeCommande(Model model){
        Client user = (Client) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Client client=clientRepository.findByEmail(user.getEmail());

        model.addAttribute("listeCommandes", client.getCommandesList());
        return "listeCommandes";
    }



}
