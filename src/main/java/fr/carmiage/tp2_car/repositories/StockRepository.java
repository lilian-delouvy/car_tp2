package fr.carmiage.tp2_car.repositories;
import fr.carmiage.tp2_car.models.Produit;
import fr.carmiage.tp2_car.models.StockProduit;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.ArrayList;
import java.util.Optional;


public interface StockRepository extends CrudRepository<StockProduit, Long> {
    @Query(" select sp from StockProduit sp " +
            " where sp.produit = ?1")
    Optional<StockProduit> findByProduct(Produit produit);

    @Query(" select sp from StockProduit sp " +
            " where sp.quantite > 0")
    Optional<ArrayList<StockProduit>> findAllWhereStockPos();
}
