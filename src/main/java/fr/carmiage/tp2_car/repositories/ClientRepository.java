package fr.carmiage.tp2_car.repositories;

import fr.carmiage.tp2_car.models.Client;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends CrudRepository<Client, Integer> {
        @Query(" select c from Client c " +
                " where c.email = ?1")
        Client findByEmail(String email);

}
