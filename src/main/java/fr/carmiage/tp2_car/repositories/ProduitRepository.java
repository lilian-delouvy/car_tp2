package fr.carmiage.tp2_car.repositories;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import fr.carmiage.tp2_car.models.Client;
import fr.carmiage.tp2_car.models.Produit;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProduitRepository extends CrudRepository<Produit, Long> {

    @Query(" select p from Produit p " +
            " where p.id = ?1")
    Optional<Produit> findById(long id);
}
