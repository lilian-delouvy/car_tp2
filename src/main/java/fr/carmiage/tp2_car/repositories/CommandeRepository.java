package fr.carmiage.tp2_car.repositories;
import fr.carmiage.tp2_car.models.Commande;
import fr.carmiage.tp2_car.models.Produit;
import fr.carmiage.tp2_car.models.StockProduit;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;


public interface CommandeRepository extends CrudRepository<Commande, Long> {

}
