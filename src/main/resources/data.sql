
INSERT INTO produit (id,nom,disponible,prix) VALUES (1,'India',TRUE,2.0);
INSERT INTO produit (id,nom,disponible,prix) VALUES (2,'Produit2',FALSE,3.0);
INSERT INTO produit (id,nom,disponible,prix) VALUES (3,'Produit3',FALSE,3.0);
INSERT INTO produit (id,nom,disponible,prix) VALUES (4,'Produit4',FALSE,3.0);
INSERT INTO produit (id,nom,disponible,prix) VALUES (5,'Produit5',FALSE,3.0);


INSERT INTO stock_produit (id, produit_id, quantite) VALUES (1, 1, 30);
INSERT INTO stock_produit (id, produit_id, quantite) VALUES (2, 2, 20);
INSERT INTO stock_produit (id, produit_id, quantite) VALUES (3, 3, 50);
INSERT INTO stock_produit (id, produit_id, quantite) VALUES (4, 4, 0);
INSERT INTO stock_produit (id, produit_id, quantite) VALUES (5, 5, 30);

insert into panier(id) values(1);
insert into client(id,email,password,panier_id) values (1,'test@test.com','$2a$10$gjV8InB8ANHjZkH3M/3pZO.Uf7ocMgpspFM4hNYApMBdRxSi7jHG6',1);
